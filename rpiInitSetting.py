#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Description:
#	- Basic default settings for RPI
#	- RASPBIAN JESSIE 2015-11-21
#	- first set things under 'import's and then run as ROOT
# Author: Martin Sakin
# Date: 2015-12-28

import sys, os
from subprocess import Popen, PIPE
import urllib	# get file

# Thinks, what you can change. False and empty string => do nothing
expandcard = True			# just reminders for expand SD card
passwdPI = 'passwordForPi'		# Password for user "pi"
passwdROOT = 'passwordForRoot'	# Password for user "root"
hostname = 'rpi'			# Setting the machine name
installList = 'vim geany python-pip sshfs openssh-server git screen'	# things I want to install
aliasfile = 'http://www.stud.fit.vutbr.cz/~xsakin00/unix/bashrc'	# my own file with aliases
pingserver = '8.8.8.8'	# server for test of connection
staterm = True			# remove pointless directory
stateupdate = True		# update system
stateupgrade = True		# upgrade system
statevim = True			# setting vim - setvim()
statewelcome = True		# edit welcome message after ssh login - logmsg()
stategen = True			# Generate ssh key
stateenroot = True		# switch to enable ssh login for "root"
#=======================================================================
#=======================================================================
numOfStep = 0
#=======================================================================
def query(text):
	sys.stdout.write(text + ' [y/n]: ')
	ch = sys.stdin.read(1)
	if ch in ['y','Y','a','A']: return True
	else: return False
#=======================================================================
def kNext():		# increase number of task
	global numOfStep
	numOfStep += 1
#=======================================================================
def k(): return (str(numOfStep) + '. ')		# int -> string
#=======================================================================
def changePassword(user, word):
	#check_call(['useradd', 'test'])
	proc=Popen(['passwd', user],stdin=PIPE,stdout=PIPE,stderr=PIPE)
	proc.stdin.write(word+'\n')
	proc.stdin.write(word)
	proc.stdin.flush()
	stdout,stderr = proc.communicate()
	#print stderr
	stderr = str(stderr)
	if ( stderr.find("password updated successfully") ) > -1:
		return True
	else:
		return False

#=======================================================================
def changeHostname(name):
	oldname = ''
	with open('/etc/hostname', 'r') as fo:
		oldname = fo.readline()
		fo.close()
	with open('/etc/hostname', 'w') as f:
		f.write(name + '\n')
		f.close()

	if os.path.isfile("/etc/hosts.save"):
		os.system("cp /etc/hosts.save /etc/hosts")
	else:
		os.system("cp /etc/hosts /etc/hosts.save")

	with open('/etc/hosts.save', 'r') as infile, open('/etc/hosts', 'w') as outfile:
		poz = 0
		for line in infile:
			if line.find('ip6-') > -1:
				continue 	# don't write (I hate IPv6)
			poz = line.find(oldname)
			if poz > -1:
				outfile.write(line[0:poz] + name + '\n')
			else:
				outfile.write(line)
#=======================================================================
def ping():
	#response = os.system("ping -c 1 " + pingserver)
	#if response == 0: return True
	#else: return False
	proc=Popen(['ping','-c','1',pingserver],stdin=PIPE,stdout=PIPE,stderr=PIPE)
	stdout,stderr = proc.communicate()
	#print stdout
	stdout = str(stdout)
	if stdout.find('0% packet loss') > -1: return True
	else: return False
#=======================================================================
def subproc(listt):		# universal subprocess
	proc=Popen(listt,stdin=PIPE,stdout=PIPE,stderr=PIPE)
	#proc.stdin.flush()
	stdout,stderr = proc.communicate()
	#print stdout
	print stderr
#=======================================================================
def getalias():		# get file with aliases from net

	urllib.urlretrieve(aliasfile, 'bashrc')
	os.system("mv bashrc /usr/bashrc")
	if not os.path.isfile("/usr/bashrc"):
		return False
	if '<html' in open('/usr/bashrc').read():
		os.system("rm /usr/bashrc")
		return False

	if not '/usr/bashrc' in open('/root/.bashrc').read():
		with open('/root/.bashrc', 'a') as f:
			f.write('\n. /usr/bashrc\n')
			f.close()

	if not '/usr/bashrc' in open('/home/pi/.bashrc').read():
		with open('/home/pi/.bashrc', 'a') as f:
			f.write('\n. /usr/bashrc\n')
			f.close()
	return True
#=======================================================================
def setvim():
	state = True
	if not os.path.isfile("/root/.vimrc"):
		with open('/root/.vimrc', 'w') as f:
			f.write("\nset bg=dark\nsyntax on\n")
			f.close()
	else: state = False
	if not os.path.isfile("/home/pi/.vimrc"):
		with open('/home/pi/.vimrc', 'w') as f:
			f.write("\nset bg=dark\nsyntax on\n")
			f.close()
	else: state = False
	return False

#=======================================================================
def keygen():
	proc=Popen(['ssh-keygen'],stdin=PIPE,stdout=PIPE,stderr=PIPE)
	proc.stdin.write('\n')
	proc.stdin.write('\n')
	proc.stdin.write('')
	proc.stdin.flush()
	stdout,stderr = proc.communicate()
	print stdout
	print stderr
#=======================================================================
def sshlogin():
	if not os.path.isfile("/etc/ssh/sshd_config.save"):
		os.system("cp /etc/ssh/sshd_config /etc/ssh/sshd_config.save")
	with open('/etc/ssh/sshd_config.save', 'r') as infile, open('/etc/ssh/sshd_config', 'w') as outfile:
		for line in infile:
			if line.find('PermitRootLogin') == 0:
				outfile.write('PermitRootLogin yes\n')
			else:
				outfile.write(line)
	os.system("service ssh restart")
#=======================================================================
def logmsg():		# zmena zpravy po prihlaseni
	if not os.path.isfile("/etc/motd.old"):
		os.system("mv /etc/motd /etc/motd.old")
		with open('/etc/motd', 'w') as f:
			f.write("Hi, I'm " + hostname + "\n")
			f.close()
		return True
	return False
#=======================================================================
def doRM():
	os.system("cd /home/pi/ && rm -fr Desktop Pictures Videos Documents Music Public Templates")
#=======================================================================
#=======================================================================
def main():

	kNext()
	if expandcard:
		if query(k() + "Did you expand the card?"):
			print(k() + 'OK')
		else:
			print(k() + "Do it! And then reboot: sudo raspi-config")
			return numOfStep

	kNext()
	if staterm:
		print(k()+'remove pointless directory')
		doRM()
		print(k() + 'OK')

	kNext()
	if passwdPI:
		print(k()+'Changing password for: pi')
		if changePassword('pi', passwdPI): print(k()+'OK')
		else: print(k()+"an error in the password change for 'pi'")
	if passwdROOT:
		print(k()+'Changing password for: root')
		if changePassword('root', passwdROOT): print(k()+'OK')
		else: print(k()+"an error in the password change for 'root'")

	kNext()
	if hostname:
		print(k()+'Changing Hostname to: ' + hostname)
		if changeHostname(hostname): print(k()+'OK')
		else: print(k()+"It has been changed, no change now.")

	kNext()
	if pingserver:
		print(k()+'Connection status:')
		if ping(): print(k()+'OK')
		else:
			print(k()+'FAIL')
			return numOfStep

	kNext()
	if stateupdate:
		print(k()+'Update')
		subproc(['apt-get', 'update'])
		print(k()+'OK')

	kNext()
	if stateupgrade:
		print(k()+'Upgrade')
		subproc(['apt-get', 'upgrade', '-y'])
		print(k()+'OK')

	kNext()
	if len(installList) != 0:
		ilist = ['apt-get', 'install', '-y']
		ilist.extend(installList.split())
		print(k()+'Install: '),
		for item in ilist:
			print item,
		subproc(ilist)
		print(k()+'OK')

	kNext()
	if aliasfile != '':
		print(k()+'Download alias list')
		if getalias(): print(k()+'OK')
		else: print(k()+'Wrong address, file not find on the net.')

	kNext()
	if statevim:
		print(k()+'Set vim')
		if setvim(): print(k()+'OK')
		else: print(k()+'Files exist, no change.')

	kNext()
	if statewelcome:
		print(k()+'Change welcome login message')
		if logmsg(): print(k()+'OK')
		else: print(k()+"It has been changed, no change now.")

	kNext()
	if stateenroot:
		print(k()+'Enable root ssh-login')
		sshlogin()
		print(k()+'OK')

	kNext()
	if stategen:
		print(k()+'Generate ssh key - Click ENTERs')
		keygen()
		print(k()+'OK')

	if query("DONE. Reboot?"):
		os.system('reboot')

#=======================================================================
if __name__ == '__main__':
	main()
