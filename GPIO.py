#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Date : 2020-02-22
# @Author : Martin Sakin, martin.sakin <at> gmail.com
#
# RPi.GPIO simulator
# Some webs: https://sourceforge.net/p/raspberry-gpio-python/wiki/Inputs/

import sys
import os
import time

import paho.mqtt.client as mqtt  # sudo -H pip3 install paho-mqtt

# Settings
MQTT_BROKER = "localhost"
MQTT_TOPIC = "gpio"
MQTT_TOPIC_INPUT = "gpio_in"
MQTT_PORT = 1883
LINUX_COLORS = True

# Do not change
NONE = 0x00
BCM = 0x01
BOARD = 0x02
IN = 0x03
OUT = 0x04
PUD_DOWN = 0x05
PUD_UP = 0x06
RISING = 0x07
FALLING = 0x08
BOTH = 0x09

pins = None

GPIO_ARRAY = ['3.3V', '5V', 2, '5V', 3, 'GND', 4, 14, 'GND', 15, 17, 18, 27, 'GND', 22, 23, '3.3V', 24, 10,
			'GND', 9, 25, 11, 8, 'GND', 7, 'I2C', 'I2C', 5, 'GND', 6, 12, 13, 'GND', 19, 16, 26, 20, 'GND', 21]

class OnePin():
	pin_number = -1
	is_set = False
	gpio = -1
	state = -1
	in_out = NONE
	pull_up_down = NONE
	event = False
	detection = NONE
	callback = None

	def _symbol(self):
		if self.state == 1:
			if LINUX_COLORS: return '\u001b[33m1\u001b[0m'
			else: return '1'
		elif self.state == 0:
			if LINUX_COLORS: return '\u001b[36m0\u001b[0m'
			else: return '0'
		elif self.is_set == True:
			if LINUX_COLORS: return '\u001b[32m~\u001b[0m'
			else: return '~'
		else:
			return '.'

	def _name(self):
		if type(self.gpio) == type(0):
			return "GPIO" + str(self.gpio)
		elif type(self.gpio) == type("0"):
			return self.gpio

class PinStorage():
	def __init__(self):
		self.mode = NONE 	# GPIO numbering
		self.pin = {}
		for i in range(1,41):
			self.pin[i] = OnePin()
			self.pin[i].pin_number = i
			self.pin[i].gpio = GPIO_ARRAY[i-1]
			if self.pin[i].gpio in ['GND']:
				self.pin[i].state = 0
			elif self.pin[i].gpio in ['3.3V', '5V']:
				self.pin[i].state = 1
			else:
				self.pin[i].state = -1

		self.writer = mqtt.Client()
		self.writer.connect(MQTT_BROKER, MQTT_PORT, 60)
		self.reader = mqtt.Client()
		self.reader.on_connect = self.on_connect_reader
		self.reader.on_message = self.requestFromMqtt
		self.reader.connect(MQTT_BROKER, MQTT_PORT, 60)
		self.reader.loop_start()

	def __del__(self):
		self.writer.disconnect()
		self.reader.disconnect()

	def postOnMqtt(self, txt):
		self.writer.publish(MQTT_TOPIC, txt)

	def on_connect_reader(self, client, userdata, flags, rc):
		if rc == 0:
			self.reader.subscribe(MQTT_TOPIC_INPUT)

	def requestFromMqtt(self, client, userdata, message):
		msg = str(message.payload.decode("utf-8"))
		num, state = msg.split()
		self.output(int(num), int(state))

	def _index(self, number):
		if not number in range(1,41):
			raise Exception("'" + str(number) + "' Out of range!")
		if self.mode == BOARD:
			return number
		elif self.mode == BCM:
			for idx,name in enumerate(GPIO_ARRAY):
				if type(name) != type("0") and name == number:
					return idx+1

	def print(self):
		msg = "#==================================#\n"
		msg += "PIN#\tNAME\tSTATE\tNAME\tPIN#"
		for i in range(1,41):
			pin = ' ' + str(i) if i > 9 else ' 0'+str(i)
			if i%2:
				msg += '\n' + pin + '\t' + self.pin[i]._name() + '\t[' + self.pin[i]._symbol() + ']'
			elif not i%2:
				msg += '[' + self.pin[i]._symbol() + ']\t' + self.pin[i]._name() + '\t' + pin
		self.postOnMqtt(msg)

	def setmode(self, mode):
		self.mode = mode

	def setup(self, number, in_out, pull_up_down):
		self.pin[self._index(number)].in_out = in_out
		self.pin[self._index(number)].pull_up_down = pull_up_down
		self.pin[self._index(number)].is_set = True
		if pull_up_down == PUD_DOWN:
			self.output(number, 0)
		elif pull_up_down == PUD_UP:
			self.output(number, 1)

	def input(self, number):
		return self.pin[self._index(number)].state

	def output(self, number, state):
		old_state = self.pin[self._index(number)].state
		if self.pin[self._index(number)].is_set and state in [1,0,True,False]:
			self.pin[self._index(number)].state = state
		if self.pin[self._index(number)].event:
			detec = self.pin[self._index(number)].detection
			func = self.pin[self._index(number)].callback
			if (detec == RISING and not old_state and state) or (detec == FALLING and old_state and not state) or (detec == BOTH):
				func(number)
		self.print()

	def add_event_detect(self, number, detection, callback, bouncetime):
		self.pin[self._index(number)].event = True
		self.pin[self._index(number)].detection = detection
		self.pin[self._index(number)].callback = callback


def setmode(mode):
	global pins
	pins = PinStorage()
	pins.setmode(mode)

def setup(pin_number, in_out, pull_up_down = NONE):
	if not in_out in [IN,OUT]:
		raise Exception('You can set only GPIO.IN or GPIO.OUT')
	global pins
	pins.setup(pin_number, in_out, pull_up_down)

def cleanup():
	pass

def input(pin_number):
	global pins
	return pins.input(pin_number)

def output(pin_number, state):
	global pins
	pins.output(pin_number, state)

def add_event_detect(number, detection = BOTH, callback = None, bouncetime = 1):
	global pins
	pins.add_event_detect(number, detection, callback, bouncetime)


'''
setmode(BCM)
setup(15,OUT)
time.sleep(2)
output(15,0)
for i in range(0,9999):
	time.sleep(0.1)
'''
